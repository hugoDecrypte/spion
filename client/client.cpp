#include "client.h"

#include <stdio.h> // perror
#include <errno.h>

#include <unistd.h>

#include <netinet/in.h>
#include <arpa/inet.h>

#include <iostream>

client::client()
{}

client::~client()
{}

void client::init()
{
  sock_creat();
  sock_connect();
}

void client::clean()
{
  close(sockfd);
}

void client::sock_creat()
{
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if(sockfd == -1)
  {
    perror("sock_creat :");
    close(sockfd);
    exit(EXIT_FAILURE);
  }
  servaddr.sin_family = AF_INET; 
  servaddr.sin_addr.s_addr = inet_addr("127.0.0.1"); 
  servaddr.sin_port = htons(1550); 
}

void client::sock_connect()
{
  if (connect(sockfd, reinterpret_cast<sockaddr *>(&servaddr), sizeof(servaddr)) != 0) {
        perror("connect :");
        exit(0);
      }
}

void client::sock_send()
{
  //get data from std::cin
  std::cin >> buffer;
  //send buffer
  send(sockfd, buffer, sizeof(buffer), 0);
  //empty buffer with bzero
  bzero(buffer, sizeof(buffer));
}
