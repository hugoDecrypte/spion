#ifndef SERVER_H
#define SERVER_H

#include <sys/types.h>
#include <sys/socket.h>

#include <netdb.h>

#include <string.h>

class client
{
public:
  client();
  ~client();

  void init();
  void clean();

  void sock_creat();
  void sock_connect();

  void sock_send();

  char buffer[1024];

private:
  int sockfd;
  struct sockaddr_in servaddr, cli;
};

#endif
