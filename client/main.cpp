#include "client.h"

#include <string.h>

int main()
{

	client client1;

	client1.init();

	while(strcmp("quit", client1.buffer) < 0)
	{
		client1.sock_send();
	}

}