#include "server.h"

#include <unistd.h>
#include <iostream>
#include <cstring>

#include <stdio.h> // perror
#include <errno.h>
#include <stdlib.h>

server::server(){

}

server::~server()
{
  clean();
}

void server::sock_creat() // would it be so bad if it closed the socket on fail just in case
{
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if(sockfd == -1)
  {
    perror("sock_creat :");
    exit(EXIT_FAILURE);
  }
}

void server::sock_opt()
{
  if(setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)))
  {
    perror("setsockopt :");
    exit(EXIT_FAILURE);
  }
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons( 1550 );
}

void server::sock_bind()
{
  if(bind(sockfd, reinterpret_cast<sockaddr *>(&address), sizeof(address))<0)
  {
    perror("bind :");
    exit(EXIT_FAILURE);
  }
}

void server::sock_listen()
{
  if(listen(sockfd, 3) < 0)
  {
    perror("listen :");
    exit(EXIT_FAILURE);
  }
}

void server::sock_accept()
{
  if((new_socket = accept(sockfd, reinterpret_cast<sockaddr *>(&address), reinterpret_cast<socklen_t *>(&addrlen))) < 0)
  {
    perror("accept :");
    exit(EXIT_FAILURE);
  }
}

int server::sock_recv()
{
  //std::cout << "sock_recv" << std::endl;
  if((valread = recv(new_socket, buffer, sizeof(buffer), 0)) > 0)
  {
    if (std::strcmp(buffer,"quit") == 0)
    {
      return -1;
    }
    std::cout << buffer << std::endl;
  }
  return valread;
}

void server::init()
{
  sock_creat();
  sock_opt();
  sock_bind();
  sock_listen();
  sock_accept();
}

void server::clean()
{
  close(new_socket);
  close(sockfd);
}
