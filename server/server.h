#ifndef SERVER_H
#define SERVER_H

#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>

class server
{
public:
  server();
  ~server();

  void init();
  void clean();

  void sock_creat();
  void sock_opt();
  void sock_bind();
  void sock_listen();
  void sock_accept();

  int sock_recv();

private:

  int new_socket, valread, sockfd, opt = 1;
  struct sockaddr_in address;
  int addrlen = sizeof(address);
  char buffer[1024]={0};
};

#endif //SERVER_H
